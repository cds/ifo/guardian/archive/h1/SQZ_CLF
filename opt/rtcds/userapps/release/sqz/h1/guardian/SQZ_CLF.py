# Nutsinee Kijbunchoo Aug 28, 2018
#
# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_CLF.py $
import sys
import time
from guardian import GuardState, GuardStateDecorator

nominal = 'LOCKED'

#############################################
#Function

def CLF_locked():
    return (ezca['SQZ-VCXO_CONTROLS_DIFFFREQUENCY']<5) and (ezca['SQZ-VCXO_CONTROLS_DIFFFREQUENCY']>-5)

#############################################
class INIT(GuardState):
    index = 0

    def main(self):
        if CLF_locked():
            return 'LOCKED'
        else:
            return 'DOWN'


class IDLE(GuardState):
    index = 3
    request = False

    def run(self):
        notify('6MHz RF Mon low or non. Check shutter, check CLF.')
        log('6MHz RF Mon low or non. Check shutter, check CLF.')
        return ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'] > -30

class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        ezca['SQZ-CLF_SERVO_IN2EN'] = 0
        ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
        ezca['SQZ-CLF_SERVO_FASTEN'] = 1 #make sure fast output is closed -- sends signal to 203 VCXO
    def run(self):
        return True

class LOCKING(GuardState):
    index = 5
    request = False

    def main(self):
        ezca['SQZ-CLF_SERVO_IN2EN'] = 1 #engage CLF locking
        ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
        #time.sleep(0.5)

    def run(self):
        if CLF_locked():     
            ezca['SQZ-CLF_SERVO_COMBOOST'] = 1
            return True
        else:
            return False

class LOCKED(GuardState):
    index = 10

    def run(self):
       

        if CLF_locked() == False:
            return 'LOCKING'
        else:
            return True

#############################################

edges = [
    ('INIT', 'IDLE'),
    ('INIT', 'DOWN'),
    ('IDLE', 'DOWN'),
    ('DOWN', 'LOCKING'),
    ('LOCKING', 'LOCKED')

]


